package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Ingredient;
import utils.Utils;
import models.Recipe;
import models.StepRecipe;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Content;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class StepRecipeController extends Controller {

    @Inject
    FormFactory formFactory;

    /****************************************************************************************************
     *
     * @param idRecipe
     * @return
     ****************************************************************************************************/
    public Result addStep(Long idRecipe){
        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.creation") );

        Form<StepRecipe> form = formFactory.form(StepRecipe.class).bindFromRequest();
        if( form.hasErrors() ){
            return Results.badRequest( form.errorsAsJson() );
        }

        StepRecipe step = form.get();
        if( step==null ) Results.internalServerError( Utils.getMsgI18n("steprecipe.error.creation") );

        List<StepRecipe> steplist = recipe.getStepsRecipe();
        if( steplist==null ) steplist = new ArrayList<>();

        step.setOrderIndex( steplist.size() );
        step.setRecipe( recipe );

        steplist.add( step );
        recipe.setStepsRecipe( steplist );

        step.save();
        recipe.update();

        return getResultByRequestEntity(request(),step);
    }


    /******************************************************************************************************
     *
     * @param id
     * @return
     *****************************************************************************************************/
    public Result retrieveStep(Long id){
        StepRecipe step = StepRecipe.findById(id);
        if( step==null ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.creation") );
        return getResultByRequestEntity(request(),step);
    }


    /******************************************************************************************************
     *
     * @param orderStep
     * @param idRecipe
     * @return
     ******************************************************************************************************/
    public Result retrieveStepByOrderRecipe(Integer orderStep, Long idRecipe){
        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.retrieveid") );

        List<StepRecipe> steps = recipe.getStepsRecipe();
        if( steps==null || steps.isEmpty() ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe") );

        if( orderStep<0 || orderStep >= recipe.getStepsRecipe().size() )
            return Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe") );

        StepRecipe step =  steps.get(orderStep);
        if( step==null  ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.retrieveorder") );

        return getResultByRequestEntity(request(), step);
    }


    /******************************************************************************************************
     *
     * @param idRecipe
     * @return
     *****************************************************************************************************/
    public Result listSteps(Long idRecipe){
        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ){
            System.out.println("SALE POR AKI");
            return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );
        }

        List<StepRecipe> steps = recipe.getStepsRecipe();
        if( steps==null || steps.isEmpty() ){
            System.out.println("SALE POR AKI");
            return Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe") );
        }

        return getResultByRequestListEntities(request(), steps);
    }

    /******************************************************************************************************
     *
     * @param idStep
     * @return
     ******************************************************************************************************/
    public Result updateStep(Long idStep){
        StepRecipe step = StepRecipe.findById(idStep);
        if( step==null ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.retrieveid") );

        Form<StepRecipe> form = formFactory.form(StepRecipe.class).bindFromRequest();
        if( form.hasErrors() ){
            return Results.badRequest(form.errorsAsJson());
        }

        StepRecipe newValue = form.get();
        if( newValue==null )  return Results.internalServerError( Utils.getMsgI18n("steprecipe.error.creation") );

        step.setDescription( newValue.getDescription() );
        step.update();

        return Results.ok( Utils.getMsgI18n("steprecipe.ok.update") );
    }


    /******************************************************************************************************
     *
     * @param orderStep
     * @param idRecipe
     * @return
     ******************************************************************************************************/
    public Result updateStepByOrderInRecipe(Integer orderStep, Long idRecipe){
        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        if( recipe.getStepsRecipe()==null || (recipe.getStepsRecipe()!=null && !recipe.getStepsRecipe().isEmpty()) ){
            Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe") );
        }
        if( orderStep<0 || orderStep>=recipe.getStepsRecipe().size() ){
            Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe2") );
        }
        StepRecipe step = recipe.getStepsRecipe().get(orderStep);
        if( step==null ) Results.notFound("steprecipe.error.retrieveorder");
        // ---------------------------------------------------------------------------
        Form<StepRecipe> form = formFactory.form(StepRecipe.class).bindFromRequest();
        if( form.hasErrors() ){
            return Results.badRequest(form.errorsAsJson());
        }
        StepRecipe newValue = form.get();
        if( newValue==null )  return Results.internalServerError( Utils.getMsgI18n("steprecipe.error.creation") );

        step.setDescription( newValue.getDescription() );
        step.update();

        return Results.ok( Utils.getMsgI18n("steprecipe.ok.update") );
    }


    /****************************************************************************************************
     *
     * @param id
     * @return
     *****************************************************************************************************/
    public Result deleteStep(Long id){
        StepRecipe step = StepRecipe.findById(id);
        if( step==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        Long idRecipe = step.getRecipe().getId();
        if( step.delete() ){
            Recipe recipe = Recipe.findById(idRecipe);
            List<StepRecipe> steps = recipe.getStepsRecipe();
            steps.remove(step);
            if( steps==null || steps.isEmpty() ) return Results.ok( Utils.getMsgI18n("steprecipe.ok.delete") );

            for(int i=0; i<steps.size(); i++){
                steps.get(i).setOrderIndex(i);
                steps.get(i).update();
            }
            recipe.setStepsRecipe( steps );

            recipe.update();
            return Results.ok( Utils.getMsgI18n("steprecipe.ok.delete2") );
        }
        return Results.internalServerError( Utils.getMsgI18n("steprecipe.error.delete") );
    }


    /****************************************************************************************************
     *
     * @param orderStep
     * @param idRecipe
     * @return
     ****************************************************************************************************/
    public Result deleteStepByOrderRecipe(Integer orderStep, Long idRecipe){
        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.creation") );

        List<StepRecipe> steps = recipe.getStepsRecipe();
        if( steps==null || steps.isEmpty() ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.listbyrecipe") );

        if( orderStep<0 || orderStep>=steps.size() )
            return Results.notFound( Utils.getMsgI18n("steprecipe.error.indexsteperror") );

        StepRecipe step = steps.get(orderStep);
        if( step==null ) return Results.notFound( Utils.getMsgI18n("steprecipe.error.retrieveorder") );

        if( step.delete() ){
            steps.remove(step);
            if( steps==null || steps.isEmpty() ) return Results.ok( Utils.getMsgI18n("steprecipe.ok.delete") );

            for(int i=0; i<steps.size(); i++){
                steps.get(i).setOrderIndex(i);
                steps.get(i).update();
            }
            recipe.setStepsRecipe( steps );

            recipe.update();
            return Results.ok( Utils.getMsgI18n("steprecipe.ok.delete2") );
        }
        return Results.internalServerError( Utils.getMsgI18n("steprecipe.error.delete") );
    }


    /****************************************************************************************************
     *
     * @param request
     * @param step
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestEntity(Http.Request request, StepRecipe step){
        switch ( Utils.getType(request) ) {

            case XML:
                Content content = views.xml.step.render(step);
                return Results.ok(content);

            case JSON:
                JsonNode json = play.libs.Json.toJson(step);
                return Results.ok(json);

            default:
                return Results.status(406);
        }

    }

    /****************************************************************************************************
     *
     * @param request
     * @param list
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestListEntities(Http.Request request, List<StepRecipe> list){
        switch ( Utils.getType(request) ) {

            case XML:
                Content content = views.xml.stepslist.render(list);
                return Results.ok(content);

            case JSON:
                JsonNode json = play.libs.Json.toJson(list);
                return Results.ok(json);

            default:
                return Results.status(406);
        }
    }

}
