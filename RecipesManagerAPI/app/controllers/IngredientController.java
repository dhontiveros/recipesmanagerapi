package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Recipe;
import org.h2.store.CountingReaderInputStream;
import utils.Utils;
import models.Ingredient;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Content;

import javax.inject.Inject;
import java.util.List;

public class IngredientController extends Controller {

    @Inject
    FormFactory formFactory;

    /****************************************************************************************************
     *
     * @return
     ****************************************************************************************************/
    public Result createIngredient(Long idRecipe){
        Recipe recipe = null;
        if( idRecipe!=null && idRecipe>=0L ){
            recipe = Recipe.findById(idRecipe);
            if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );
        }

        Form<Ingredient> form = formFactory.form(Ingredient.class).bindFromRequest();
        if( form.hasErrors() ){
            return Results.badRequest(form.errorsAsJson());
        }
        Ingredient ingredient = form.get();
        if( ingredient==null ) return Results.internalServerError( Utils.getMsgI18n("ingredient.error.creation") );

        ingredient.setName( ingredient.getName().toUpperCase() );

        Ingredient anotherSame = Ingredient.findByName( ingredient.getName() );
        if( anotherSame!=null ){
            return Results.status(409, Utils.getMsgI18n("ingredient.error.creation.same"));
        }

        ingredient.save();
        if( recipe!=null ){
            recipe.addIngredient(ingredient);
            recipe.update();
        }
        return getResultByRequestEntity(request(),ingredient);
    }

    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result retrieveIngredient(Long id) {
        Ingredient ingredient = Ingredient.findById(id);
        if( ingredient==null ){
            return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveid") );
        }
        return getResultByRequestEntity(request(),ingredient);
    }

    /****************************************************************************************************
     *
     * @param nameIngredient
     * @return
     ****************************************************************************************************/
    public Result retrieveIngredientByName(String nameIngredient){
        Ingredient ingredient = Ingredient.findByName(nameIngredient.trim());
        if( ingredient==null ){
            return Results.notFound( Utils.getMsgI18n("ingredient.error.retrievename") );
        }
        return getResultByRequestEntity(request(),ingredient);
    }

    /****************************************************************************************************
     *
     * @return
     ****************************************************************************************************/
    public Result listIngredients(){
        List<Ingredient> list = Ingredient.findAll();
        if( list==null || list.isEmpty() ) return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveall") );
        return getResultByRequestListEntities(request(), list);
    }

    /****************************************************************************************************
     *
     * @param idIngredient
     * @param idRecipe
     * @return
     ****************************************************************************************************/
    public Result addIngredientToRecipe(Long idIngredient, Long idRecipe){
        Ingredient ingredient = Ingredient.findById(idIngredient);
        if( ingredient==null ) return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveid") );

        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        if( recipe.getIngredients().contains(ingredient) )
            return Results.status(409, Utils.getMsgI18n("ingredient.error.existinrecipe") );

        recipe.addIngredient(ingredient);
        recipe.update();

        return Results.ok(Utils.getMsgI18n("ingredient.ok.addrecipe"));
    }

    /****************************************************************************************************
     *
     * @param idIngredient
     * @param idRecipe
     * @return
     ****************************************************************************************************/
    public Result removeIngredientFromRecipe(Long idIngredient, Long idRecipe){
        Ingredient ingredient = Ingredient.findById(idIngredient);
        if( ingredient==null ) return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveid") );

        Recipe recipe = Recipe.findById(idRecipe);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        if( !recipe.getIngredients().contains(ingredient) )
            return Results.status(409, Utils.getMsgI18n("ingredient.error.noexistsinrecipe") );

        recipe.removeIngredient(ingredient);
        recipe.update();

        return Results.ok(Utils.getMsgI18n("ingredient.ok.removerecipe"));
    }

    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result updateIngredient(Long id){
        Ingredient ingredient = Ingredient.findById(id);
        if( ingredient==null ) return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveid") );

        Form<Ingredient> form = formFactory.form(Ingredient.class).bindFromRequest();
        if( form.hasErrors() ){
            return Results.badRequest(form.errorsAsJson());
        }
        Ingredient newValue = form.get();
        if( newValue==null ) return Results.internalServerError( Utils.getMsgI18n("ingredient.error.creation") );

        if( newValue.getName()!=null && !newValue.getName().equals(Ingredient.EMPTY_FIELD) ){
            ingredient.setName( newValue.getName().toUpperCase() );
        }
        if( newValue.getDescription()!=null && !newValue.getDescription().isEmpty() ){
            ingredient.setDescription( newValue.getDescription() );
        }
        ingredient.update();

        return Results.ok(Utils.getMsgI18n("ingredient.ok.update") );
    }


    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result deleteIngredient(Long id){
        Ingredient ingredient = Ingredient.findById(id);
        if( ingredient==null ) return Results.notFound( Utils.getMsgI18n("ingredient.error.retrieveid") );

        // 409 - CONFLICT ACTUAL RESOURCE
        if( ingredient.getRecipes()!=null && !ingredient.getRecipes().isEmpty() )
            return Results.status(409, Utils.getMsgI18n("ingredient.error.deletefromrecipe") );

        if( ingredient.delete() ) return Results.ok( Utils.getMsgI18n("ingredient.ok.delete") );
        return Results.internalServerError( Utils.getMsgI18n("ingredient.error.delete") );
    }

// =====================================================================================================================


    /****************************************************************************************************
     *
     * @param request
     * @param ingredient
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestEntity(Http.Request request, Ingredient ingredient){
        switch ( Utils.getType(request) ) {

            case XML:
                Content content = views.xml.ingredient.render(ingredient);
                return Results.ok(content);
            case JSON:
                JsonNode json = play.libs.Json.toJson(ingredient);
                return Results.ok(json);

            default:
                return Results.status(406);
        }

    }

    /****************************************************************************************************
     *
     * @param request
     * @param list
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestListEntities(Http.Request request, List<Ingredient> list){
        switch ( Utils.getType(request) ) {

            case XML:
                Content content = views.xml.ingredientslist.render(list);
                return Results.ok(content);
            case JSON:
                JsonNode json = play.libs.Json.toJson(list);
                return Results.ok(json);

            default:
                return Results.status(406);
        }
    }

}
