package controllers;

import models.StepRecipe;
import utils.Utils;
import models.Ingredient;
import models.Recipe;
import com.fasterxml.jackson.databind.JsonNode;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import play.twirl.api.Content;
import validators.recipe.DifficultyValidator;
import validators.recipe.TypeCourseValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class RecipeController extends Controller {

    @Inject
    FormFactory formFactory;

    /****************************************************************************************************
     *
     * @return
     ****************************************************************************************************/
    public Result createRecipe(){
        Form<Recipe> form = formFactory.form(Recipe.class).bindFromRequest(request());
        if (form.hasErrors()) {
            return Results.badRequest(form.errorsAsJson());
        }
        Recipe recipe = form.get();

        if( recipe==null ) return Results.internalServerError( Utils.getMsgI18n("recipe.error.creation") );

        if( Recipe.findByName( recipe.getName() )!=null ) return Results.status(409, Utils.getMsgI18n(""));

        List<Ingredient> ingredientsToCreate = recipe.getIngredients();
        recipe.setIngredients(new ArrayList<>());

        for (Ingredient ingredient : ingredientsToCreate) {
            Ingredient ingredientInDB = Ingredient.findByName(ingredient.getName());
            if (ingredientInDB != null) {
                recipe.addIngredient(ingredientInDB);
            } else {
                ingredient.setName(ingredient.getName().toUpperCase());
                ingredient.save();
                recipe.addIngredient(ingredient);
            }
        }

        recipe.setName( recipe.getName().toUpperCase() );
        recipe.save();

        // Será necesario indicar el índice de ordenación para cada paso.
        List<StepRecipe> stepsToCreate = recipe.getStepsRecipe();
        for(int i=0; i<stepsToCreate.size(); i++){
            stepsToCreate.get(i).setOrderIndex(i);
            stepsToCreate.get(i).setRecipe(recipe);
            stepsToCreate.get(i).save();
        }
        recipe.setStepsRecipe(stepsToCreate);

        return getResultByRequestEntity(request(),recipe);
    }


    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result retrieveRecipe(Long id) {
        Recipe recipe = Recipe.findById(id);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );
        return getResultByRequestEntity(request(),recipe);
    }

    /****************************************************************************************************
     *
     * @param name
     * @return
     ****************************************************************************************************/
    public Result retrieveRecipeByName(String name){
        Recipe recipe = Recipe.findByName(name);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrievename") );
        return getResultByRequestEntity(request(),recipe);
    }

    /****************************************************************************************************
     *
     * @param numOfPeople
     * @return
     ****************************************************************************************************/
    public Result retrieveRecipesByNumOfPeople(Integer numOfPeople){
        if( numOfPeople==null || ( numOfPeople!=null && (numOfPeople<Recipe.MIN_VALUE_NUMPEOPLE || numOfPeople>Recipe.MAX_VALUE_NUMPEOPLE) ) )
            return Results.badRequest(Utils.getMsgI18n("recipe.validator.numpeople"));

        List<Recipe> list = Recipe.findByNumPeople(numOfPeople);
        if( list==null || list.isEmpty() ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveall") );
        return getResultByRequestListEntities(request(), list);
    }

    /****************************************************************************************************
     *
     * @param typeCourse
     * @return
     ****************************************************************************************************/
    public Result  retrieveRecipesByTypeOfCourse(Integer typeCourse){
        if( typeCourse==null || (new TypeCourseValidator().checkTypeCourse(typeCourse)) )
            return Results.badRequest(Utils.getMsgI18n("recipe.validator.typecourse"));

        List<Recipe> list = Recipe.findByTypeCourse(typeCourse);
        if( list==null || list.isEmpty() ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveall") );
        return getResultByRequestListEntities(request(), list);
    }

    /****************************************************************************************************
     *
     * @param difficulty
     * @return
     ****************************************************************************************************/
    public Result retrieveRecipesByDifficulty(Integer difficulty){
        if( difficulty==null || (new DifficultyValidator().checkDifficulty(difficulty)) )
            return Results.badRequest(Utils.getMsgI18n("recipe.validator.difficulty"));

        List<Recipe> list = Recipe.findByDifficulty(difficulty);
        if( list==null || list.isEmpty() ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveall") );
        return getResultByRequestListEntities(request(), list);
    }

    /****************************************************************************************************
     *
     * @return
     ****************************************************************************************************/
    public Result listRecipes(){
        List<Recipe> list = Recipe.findAll();
        if( list==null || list.isEmpty() ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveall") );
        return getResultByRequestListEntities(request(), list);
    }

    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result updateRecipe(Long id){
        Recipe recipe = Recipe.findById(id);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        Form<Recipe> form = formFactory.form(Recipe.class).bindFromRequest(request());
        if (form.hasErrors()) {
            return Results.badRequest(form.errorsAsJson());
        }
        Recipe newValue = form.get();
        if( newValue==null ) return Results.internalServerError( Utils.getMsgI18n("recipe.error.creation") );

        recipe = newValue;
        recipe.update();

        return getResultByRequestEntity(request(),recipe);
    }

    /****************************************************************************************************
     *
     * @param id
     * @return
     ****************************************************************************************************/
    public Result deleteRecipe(Long id){
        Recipe recipe = Recipe.findById(id);
        if( recipe==null ) return Results.notFound( Utils.getMsgI18n("recipe.error.retrieveid") );

        List<StepRecipe> steps = recipe.getStepsRecipe();
        if( steps!=null && !steps.isEmpty() ){
            for(StepRecipe step : steps){
                step.delete();
            }
        }

        if( recipe.delete() ) return Results.ok( Utils.getMsgI18n("recipe.ok.delete") );
        return Results.internalServerError( Utils.getMsgI18n("recipe.error.delete") );
    }

// =====================================================================================================================

    /****************************************************************************************************
     *
     * @param request
     * @param recipe
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestEntity(Http.Request request, Recipe recipe){
        switch ( Utils.getType(request) ) {
            case XML:
                Content content = views.xml.recipe.render(recipe);
                return Results.ok(content);
            case JSON:
                JsonNode json = play.libs.Json.toJson(recipe);
                return Results.ok(json);
            default:
                return Results.status(406);
        }
    }

    /****************************************************************************************************
     *
     * @param request
     * @param list
     * @return
     ****************************************************************************************************/
    private Result getResultByRequestListEntities(Http.Request request, List<Recipe> list){
        switch ( Utils.getType(request) ) {
            case XML:
                Content content = views.xml.recipeslist.render(list);
                return Results.ok(content);
            case JSON:
                JsonNode json = play.libs.Json.toJson(list);
                return Results.ok(json);
            default:
                return Results.status(406);
        }
    }
}
