package validators.step;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DescriptionStepValidator extends Constraints.Validator<String> implements ConstraintValidator<DescriptionStep,String> {

    private static final Integer MIN_LENGTH = 5;
    private static final Integer MAX_LENGTH = 200;

    @Override
    public boolean isValid(String descriptionStep) {
        return this.isValid(descriptionStep, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("steprecipe.validator.description"), new Object[]{""}
        );
    }

    @Override
    public void initialize(DescriptionStep constraintAnnotation) {

    }

    @Override
    public boolean isValid(String descriptionStep, ConstraintValidatorContext context) {
        if( descriptionStep!=null ){
            return ( !descriptionStep.isEmpty() && descriptionStep.length()>MIN_LENGTH && descriptionStep.length()<=MAX_LENGTH );
        }
        return false;
    }
}
