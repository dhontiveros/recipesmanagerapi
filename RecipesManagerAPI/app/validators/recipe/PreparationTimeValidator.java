package validators.recipe;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PreparationTimeValidator extends Constraints.Validator<String> implements ConstraintValidator<PreparationTime,String> {

    @Override
    public boolean isValid(String preparationTime) {
        return this.isValid(preparationTime, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("recipe.validator.preparationtime"), new Object[]{""}
        );
    }

    @Override
    public void initialize(PreparationTime constraintAnnotation) {

    }

    @Override
    public boolean isValid(String preparationTime, ConstraintValidatorContext context) {
        return ( preparationTime==null || (preparationTime!=null && checkPreparationTime(preparationTime)) );
    }

    private boolean checkPreparationTime(String preparationTime){
        return( preparationTime.contains("h") || preparationTime.contains("H") ||
                preparationTime.contains("m") || preparationTime.contains("min") ||
                preparationTime.contains("s") || preparationTime.contains("secs") || preparationTime.contains("segs") );
    }
}
