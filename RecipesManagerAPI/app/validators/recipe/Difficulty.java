package validators.recipe;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DifficultyValidator.class)
public @interface Difficulty {
    String message() default "recipe.validator.difficulty";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default {};
}