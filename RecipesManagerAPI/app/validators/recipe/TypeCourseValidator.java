package validators.recipe;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TypeCourseValidator extends Constraints.Validator<Integer> implements ConstraintValidator<TypeCourse,Integer>{


    @Override
    public boolean isValid(Integer typeCourse) {
        return this.isValid(typeCourse, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("recipe.validator.typecourse"), new Object[]{});
    }

    @Override
    public void initialize(TypeCourse constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer typeCourse, ConstraintValidatorContext context) {
        return checkTypeCourse(typeCourse);
    }

    public boolean checkTypeCourse(Integer typeCourse){
        return typeCourse!=null && typeCourse>=0 && typeCourse<=2;
    }
}
