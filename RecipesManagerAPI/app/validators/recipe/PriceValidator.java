package validators.recipe;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PriceValidator extends Constraints.Validator<Float> implements ConstraintValidator<Price,Float> {

    @Override
    public boolean isValid(Float price) {
        return this.isValid(price, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("recipe.validator.price"), new Object[]{""}
        );
    }

    @Override
    public void initialize(Price constraintAnnotation) {

    }

    @Override
    public boolean isValid(Float price, ConstraintValidatorContext context) {
        return ( price==null || (price!=null && price>=0f) );
    }
}
