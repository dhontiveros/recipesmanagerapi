package validators.recipe;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DifficultyValidator extends Constraints.Validator<Integer> implements ConstraintValidator<Difficulty,Integer> {

    @Override
    public boolean isValid(Integer difficulty) {
        return this.isValid(difficulty, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("recipe.validator.difficulty"), new Object[]{""}
        );
    }

    @Override
    public void initialize(Difficulty constraintAnnotation) {

    }


    /**
     *  0 - Fácil
     *  1 - Medio
     *  2 - Difícil
     * @param difficulty
     * @param context
     * @return
     */
    @Override
    public boolean isValid(Integer difficulty, ConstraintValidatorContext context) {
        return checkDifficulty(difficulty);
    }


    public boolean checkDifficulty(Integer difficulty){
        return (difficulty!=null && difficulty>=0 && difficulty<=2);
    }
}
