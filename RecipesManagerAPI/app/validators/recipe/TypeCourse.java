package validators.recipe;

import utils.Utils;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TypeCourseValidator.class)
public @interface TypeCourse {
    String message() default "recipe.validator.typecourse";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default {};
}
