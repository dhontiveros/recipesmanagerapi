package validators.ingredient;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NameIngredientValidator.class)
public @interface NameIngredient {
    String message() default "ingredient.validator.name";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default {};
}