package validators.ingredient;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameIngredientValidator extends Constraints.Validator<String> implements ConstraintValidator<NameIngredient,String> {

    private static final Integer MIN_LENGTH = 3;
    private static final Integer MAX_LENGTH = 50;


    @Override
    public boolean isValid(String nameIngredient) {
        return this.isValid(nameIngredient, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("ingredient.validator.name"), new Object[]{""});
    }

    @Override
    public void initialize(NameIngredient constraintAnnotation) {

    }

    @Override
    public boolean isValid(String nameIngredient, ConstraintValidatorContext context) {
        if( nameIngredient!=null ){
            return ( !nameIngredient.isEmpty() && nameIngredient.length()>=MIN_LENGTH && nameIngredient.length()<=MAX_LENGTH );
        }
        return false;
    }
}
