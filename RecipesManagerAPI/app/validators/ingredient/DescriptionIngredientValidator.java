package validators.ingredient;

import play.data.validation.Constraints;
import play.libs.F;
import utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DescriptionIngredientValidator extends Constraints.Validator<String> implements ConstraintValidator<DescriptionIngredient,String> {

    private static final Integer MIN_LENGTH = 5;
    private static final Integer MAX_LENGTH = 100;

    @Override
    public boolean isValid(String descriptionIngredient) {
        return this.isValid(descriptionIngredient, null);
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<>(Utils.getMsgI18n("ingredient.validator.description"), new Object[]{""});
    }

    @Override
    public void initialize(DescriptionIngredient constraintAnnotation) {

    }

    @Override
    public boolean isValid(String descriptionIngredient, ConstraintValidatorContext context) {
        if( descriptionIngredient!=null ){
            return ( !descriptionIngredient.isEmpty() && descriptionIngredient.length()>=MIN_LENGTH && descriptionIngredient.length()<=MAX_LENGTH );
        }
        return false;
    }
}
