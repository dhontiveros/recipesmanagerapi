package utils;

import play.mvc.Http;
import play.mvc.Http.Request;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    private static final String JSON_STRING_TYPE    = "application/json";
    private static final String XML_STRING_TYPE     = "application/xml";

    private static Map<String, TypeContentNeg> contentTypesList = null;


    /****************************************************************************************************
     *
     ****************************************************************************************************/
    private static void loadContentTypesList()
    {
        contentTypesList = new HashMap();
        contentTypesList.put(JSON_STRING_TYPE,  TypeContentNeg.JSON);
        contentTypesList.put(XML_STRING_TYPE,   TypeContentNeg.XML);
    }

    /****************************************************************************************************
     *
     * @param request
     * @return
     ****************************************************************************************************/
    public static TypeContentNeg getType(Request request){
        if( contentTypesList==null ) loadContentTypesList();

        for (Map.Entry<String, TypeContentNeg> entry : contentTypesList.entrySet()) {
            if( request.accepts(entry.getKey()) ){
                return entry.getValue();
            }
        }
        return TypeContentNeg.NONE;
    }


    /***************************************************************************************************
     *
     * @param key
     * @return
     ***************************************************************************************************/
    public static String getMsgI18n(String key){
        return Http.Context.current().messages().at(key);
    }

}


