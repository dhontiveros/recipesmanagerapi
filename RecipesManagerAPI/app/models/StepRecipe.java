package models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.*;
import validators.step.DescriptionStep;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StepRecipe extends Model {

    public static final Finder<Long, StepRecipe> find = new Finder<>(StepRecipe.class);

    @Id
    private Long id;

    @JsonIgnore
    private Integer orderIndex;

    @Required
    @DescriptionStep
    private String description;

    @ManyToOne
    @JsonBackReference
    private Recipe recipe;

    public StepRecipe(){
        super();
    }

    public Long     getId()             { return id; }
    public Integer  getOrderIndex()     { return orderIndex; }
    public String   getDescription()    { return description; }
    public Recipe   getRecipe()         { return recipe; }

    public void setId(Long id)                      { this.id = id; }
    public void setOrderIndex(Integer orderIndex)   { this.orderIndex = orderIndex; }
    public void setDescription(String description)  { this.description = description; }
    public void setRecipe(Recipe recipe)            { this.recipe = recipe; }

    public static StepRecipe findById(Long id) {
        return find.byId(id);
    }

}
