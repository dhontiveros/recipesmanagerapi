package models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.JsonIgnore;
import io.ebean.annotation.UpdatedTimestamp;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import validators.recipe.Difficulty;
import validators.recipe.PreparationTime;
import validators.recipe.Price;
import validators.recipe.TypeCourse;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Recipe extends Model{

    public static final int MIN_VALUE_NUMPEOPLE = 1;
    public static final int MAX_VALUE_NUMPEOPLE = 10;

    public static final Finder<Long,Recipe> find = new Finder<>(Recipe.class);

    @Id private Long id;

    // ------------------------------------------------------------
    // INFORMATION ATTRIBUTES
    // ------------------------------------------------------------
    // @Required(message="validation.clave.email")
    @Required(message="recipe.constraint.name.required")
    @MinLength(message="recipe.constraint.name.min", value=5)
    @MaxLength(message="recipe.constraint.name.max", value=50)
    private String name;

    @Required(message="recipe.constraint.desc.required")
    @MinLength(message="recipe.constraint.desc.min", value=5)
    @MaxLength(message="recipe.constraint.desc.max", value=300)
    private String description;

    @Required(message="recipe.constraint.numPeople.required")
    @Min(message="recipe.constraint.numPeople.min", value=MIN_VALUE_NUMPEOPLE)
    @Max(message="recipe.constraint.numPeople.max", value=MAX_VALUE_NUMPEOPLE)
    private Integer numOfPeople;

    @TypeCourse
    private Integer typeCourse;

    @PreparationTime
    private String preparationTime;

    @Price
    private Float price;

    @Difficulty
    private Integer difficulty;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Ingredient> ingredients;

    @OneToMany(cascade= CascadeType.ALL, mappedBy="recipe")
    @JsonManagedReference
    private List<StepRecipe> stepsRecipe;

    // ------------------------------------------------------------
    // PERSISTENCE ATTRIBUTES
    // ------------------------------------------------------------
    @CreatedTimestamp
    Timestamp whenCreated;

    @UpdatedTimestamp
    Timestamp whenUpdate;

    public Recipe(){
        super();
    }

    public static Recipe        findById            (Long id)               { return find.byId(id); }
    public static Recipe        findByName          (String name)           { return find.query().where().eq("name", name.toUpperCase()). findOne(); }
    public static List<Recipe>  findByTypeCourse    (Integer typeCourse)    { return find.query().where().eq("typeCourse", typeCourse).findList(); }
    public static List<Recipe>  findByNumPeople     (Integer numOfPeople)   { return find.query().where().eq("numOfPeople", numOfPeople).findList(); }
    public static List<Recipe>  findByDifficulty    (Integer difficulty)    { return find.query().where().eq("difficulty", difficulty).findList(); }

    public static List<Recipe>  findAll()       { return find.all();    }

    public Long     getId()                     { return id;                }
    public String   getName()                   { return name;              }
    public String   getDescription()            { return description;       }
    public Integer  getNumOfPeople()            { return numOfPeople;       }
    public Integer  getDifficulty()             { return difficulty;        }

    public Integer  getTypeCourse()             { return typeCourse;        }
    public String   getPreparationTime()        { return preparationTime;   }
    public Float    getPrice()                  { return price;             }

    public List<Ingredient> getIngredients()    { return ingredients;       }
    public List<StepRecipe> getStepsRecipe()    { return stepsRecipe;       }

    public Timestamp getWhenCreated()           { return whenCreated;       }
    public Timestamp getWhenUpdate()            { return whenUpdate;        }


    public void setId(Long id)                                  {  this.id              = id;           }
    public void setName(String name)                            { this.name             = name;         }
    public void setDescription(String description)              { this.description      = description;  }
    public void setNumOfPeople(Integer numOfPeople)             { this.numOfPeople      = numOfPeople;  }
    public void setDifficulty(Integer difficulty)               { this.difficulty       = difficulty;   }

    public void setTypeCourse(Integer typeCourse)               { this.typeCourse       = typeCourse;       }
    public void setPreparationTime(String preparationTime)      { this.preparationTime  = preparationTime;  }
    public void setPrice(Float price)                           { this.price            = price;            }

    public void setIngredients(List<Ingredient> ingredients)    { this.ingredients      = ingredients;      }
    public void setStepsRecipe(List<StepRecipe> stepsRecipe)    { this.stepsRecipe      = stepsRecipe;      }


    public void addIngredient(Ingredient i){
        this.ingredients.add(i);
        i.getRecipes().add(this);
    }

    public void removeIngredient(Ingredient i){
        this.ingredients.remove(i);
        i.getRecipes().remove(this);
    }
}
