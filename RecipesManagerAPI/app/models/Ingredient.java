package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.*;
import validators.ingredient.NameIngredient;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Ingredient extends Model {

    public static final String EMPTY_FIELD = "---";

    public static final Finder<Long,Ingredient> find = new Finder<>(Ingredient.class);

    public static Ingredient findByName(String name) {
        return find.query().where().eq("name", name.toUpperCase()). findOne();
    }

    @Id
    private Long id;

    @Required
    @NameIngredient
    private String name;

    private String description;

    @ManyToMany(mappedBy = "ingredients")
    @JsonBackReference
    private List<Recipe> recipes;

    public Ingredient(){
        super();
    }

    public static Ingredient findById(Long id) {
        return find.byId(id);
    }

    public static List<Ingredient> findAll(){
        return find.all();
    }

    public Long             getId()             { return id;            }
    public String           getName()           { return name;          }
    public String           getDescription()    { return description;   }
    public List<Recipe>     getRecipes()        { return recipes;       }

    public void setId           (Long id)               { this.id           = id;           }
    public void setName         (String name)           { this.name         = name;         }
    public void setDescription  (String description)    { this.description  = description;  }
    public void setRecipes      (List<Recipe> recipes)  { this.recipes      = recipes;      }
}
