# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table ingredient (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  description                   varchar(255),
  constraint pk_ingredient primary key (id)
);

create table recipe (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  description                   varchar(255),
  num_of_people                 integer,
  type_course                   integer,
  preparation_time              varchar(255),
  price                         float,
  difficulty                    integer,
  when_created                  timestamp not null,
  when_update                   timestamp not null,
  constraint pk_recipe primary key (id)
);

create table recipe_ingredient (
  recipe_id                     bigint not null,
  ingredient_id                 bigint not null,
  constraint pk_recipe_ingredient primary key (recipe_id,ingredient_id)
);

create table step_recipe (
  id                            bigint auto_increment not null,
  order_index                   integer,
  description                   varchar(255),
  recipe_id                     bigint,
  constraint pk_step_recipe primary key (id)
);

create index ix_recipe_ingredient_recipe on recipe_ingredient (recipe_id);
alter table recipe_ingredient add constraint fk_recipe_ingredient_recipe foreign key (recipe_id) references recipe (id) on delete restrict on update restrict;

create index ix_recipe_ingredient_ingredient on recipe_ingredient (ingredient_id);
alter table recipe_ingredient add constraint fk_recipe_ingredient_ingredient foreign key (ingredient_id) references ingredient (id) on delete restrict on update restrict;

create index ix_step_recipe_recipe_id on step_recipe (recipe_id);
alter table step_recipe add constraint fk_step_recipe_recipe_id foreign key (recipe_id) references recipe (id) on delete restrict on update restrict;


# --- !Downs

alter table recipe_ingredient drop constraint if exists fk_recipe_ingredient_recipe;
drop index if exists ix_recipe_ingredient_recipe;

alter table recipe_ingredient drop constraint if exists fk_recipe_ingredient_ingredient;
drop index if exists ix_recipe_ingredient_ingredient;

alter table step_recipe drop constraint if exists fk_step_recipe_recipe_id;
drop index if exists ix_step_recipe_recipe_id;

drop table if exists ingredient;

drop table if exists recipe;

drop table if exists recipe_ingredient;

drop table if exists step_recipe;

