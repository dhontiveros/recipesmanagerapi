# ---------------------------------------------------------------------------------------------------------
# RECIPES
# ---------------------------------------------------------------------------------------------------------
POST /recipe controllers.RecipeController.createRecipe()
GET /recipe/:id controllers.RecipeController.retrieveRecipe(id: Long)
GET /recipe/:name controllers.RecipeController.retrieveRecipeByName(name: String)
GET /recipepeople/:nPeople controllers.RecipeController.retrieveRecipesByNumOfPeople(nPeople: Integer)
GET /recipecourse/:tCourse controllers.RecipeController.retrieveRecipesByTypeOfCourse(tCourse: Integer)
GET /recipedifficulty/:diffic controllers.RecipeController.retrieveRecipesByDifficulty(diffic: Integer)
GET /recipes controllers.RecipeController.listRecipes()
PUT /recipe/:id controllers.RecipeController.updateRecipe(id: Long)
DELETE /recipe/:id controllers.RecipeController.deleteRecipe(id: Long)

# ---------------------------------------------------------------------------------------------------------
# INGREDIENTS
# ---------------------------------------------------------------------------------------------------------
POST /ingredient/:idRecipe controllers.IngredientController.createIngredient(idRecipe: Long)
GET /ingredient/:id controllers.IngredientController.retrieveIngredient(id: Long)
GET /ingredientname/:name controllers.IngredientController.retrieveIngredientByName(name: String)
GET /ingredients controllers.IngredientController.listIngredients()
GET /addingredientrecipe/:idIng/:idRec controllers.IngredientController.addIngredientToRecipe(idIng: Long, idRec: Long)
GET /removeingredientrecipe/:idIng/:idRec controllers.IngredientController.removeIngredientFromRecipe(idIng: Long, idRec: Long)
PUT /ingredient/:id controllers.IngredientController.updateIngredient(id: Long)
DELETE /ingredient/:id controllers.IngredientController.deleteIngredient(id: Long)

# ---------------------------------------------------------------------------------------------------------
# STEPS
# ---------------------------------------------------------------------------------------------------------
POST /step/:idRecipe controllers.StepRecipeController.addStep(idRecipe: Long)
GET /step/:id controllers.StepRecipeController.retrieveStep(id: Long)
GET /step/:orderStep/:idRecipe controllers.StepRecipeController.retrieveStepByOrderRecipe(orderStep: Integer, idRecipe: Long)
GET /steps/:idRecipe controllers.StepRecipeController.listSteps(idRecipe: Long)
PUT /step/:idStep controllers.StepRecipeController.updateStep(idStep: Long)
PUT /step/:orderStep/:idRecipe controllers.StepRecipeController.updateStepByOrderInRecipe(orderStep: Integer, idRecipe: Long)
DELETE /step/:id controllers.StepRecipeController.deleteStep(id: Long)
DELETE /step/:orderStep/:idRecipe controllers.StepRecipeController.deleteStepByOrderRecipe(orderStep: Integer, idRecipe: Long)
