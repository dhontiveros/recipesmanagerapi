package utils;

import models.Ingredient;
import models.Recipe;
import models.StepRecipe;

import java.util.ArrayList;

public abstract class TestUtils {


    public static final String RECIPE_CREATE_JSON       = "createRecipe.json";
    public static final String INGREDIENT_CREATE_JSON   = "createIngredient.json";
    public static final String STEP_ADD_JSON            = "addStep.json";


    public static final String HEADER_CONTENT_TYPE  = "Content-type";
    public static final String HEADER_ACCEPT        = "Accept";
    public static final String TYPE_JSON            = "application/json";
    public static final String TYPE_XML             = "application/xml";


    public static Recipe createRecipeFromNumber(Integer num){
        // -----------------------------------------
        Recipe recipe = new Recipe();
        recipe.setName("Nombre de receta test "+num);
        recipe.setDescription("Descripción de la receta de test "+num);
        recipe.setDifficulty(0);
        recipe.setPreparationTime("30 min");
        recipe.setNumOfPeople(4);
        recipe.setPrice(27.5f);
        recipe.setTypeCourse(1);
        // -----------------------------------------
        recipe.addIngredient(createIngredientByNumber(1,false));
        recipe.addIngredient(createIngredientByNumber(2,false));
        // -----------------------------------------
        recipe.save();

        StepRecipe sr1 = createStepByName("Primer paso de la receta", 0, recipe);
        StepRecipe sr2 = createStepByName("Segundo paso de la receta", 1, recipe);

        recipe.setStepsRecipe(new ArrayList<>());
        recipe.getStepsRecipe().add(sr1);
        recipe.getStepsRecipe().add(sr2);

        return recipe;

        // -----------------------------------------
    }

    public static Ingredient createIngredientByNumber(int num, boolean saving){
        Ingredient ing = createIngredientByNumber(num);
        if( saving ) ing.save();
        return ing;
    }

    public static Ingredient createIngredientByNumber(int num){
        Ingredient ingredient = new Ingredient();
        ingredient.setName("Nombre ingredient test "+num);
        ingredient.setDescription("Descripción del ingrediente test "+num);
        return ingredient;
    }


    public static StepRecipe createStepByName(String description, int index, Recipe recipe){
        StepRecipe stepRecipe = new StepRecipe();
        stepRecipe.setOrderIndex(index);
        stepRecipe.setDescription(description);
        stepRecipe.setRecipe(recipe);
        stepRecipe.save();
        return stepRecipe;
    }
}
