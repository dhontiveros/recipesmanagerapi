package controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.routes;
import models.Recipe;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.util.StringUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import play.Application;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import play.twirl.api.Content;
import scala.io.Codec;
import scala.io.Source;
import utils.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RecipeTest extends WithApplication {

    private static final String ROUTE_RECIPE  = "/recipe";
    private static final String ROUTE_RECIPES = "/recipes";

    private List<Recipe> recipesList;

    @Override
    protected Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }


    @Before
    public void setUpStepRecipe(){
        recipesList = new ArrayList<>();
        recipesList.add( TestUtils.createRecipeFromNumber(1) );
        recipesList.add( TestUtils.createRecipeFromNumber(2) );
    }

    @After
    public void tearDownStepRecipe(){
        if( recipesList!=null && !recipesList.isEmpty() ){
            for(Recipe recipe : recipesList){
                recipe.delete();
            }
        }
        recipesList.clear();
        recipesList = null;
    }

    @Test
    public void createRecipeJSONTest() throws IOException {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        String  jsonString  = Source.fromURL( getClass().getClassLoader().getResource(TestUtils.RECIPE_CREATE_JSON), Codec.defaultCharsetCodec() ).getLines().mkString();
        JsonNode jsonNode = (new ObjectMapper()).readTree(jsonString);
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header(TestUtils.HEADER_CONTENT_TYPE,  TestUtils.TYPE_JSON)
                .header(TestUtils.HEADER_ACCEPT,        TestUtils.TYPE_JSON)
                .bodyJson(jsonNode)
                .uri(routes.RecipeController.createRecipe().url());

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        testRecipeJSONAttrs( result );
        // --------------------------------------------------------------
    }


    @Test
    public void createRecipeXMLTest() throws IOException{
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        String  jsonString  = Source.fromURL( getClass().getClassLoader().getResource(TestUtils.RECIPE_CREATE_JSON), Codec.defaultCharsetCodec() ).getLines().mkString();
        JsonNode jsonNode = (new ObjectMapper()).readTree(jsonString);
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header(TestUtils.HEADER_CONTENT_TYPE,  TestUtils.TYPE_JSON)
                .header(TestUtils.HEADER_ACCEPT,        TestUtils.TYPE_XML)
                .bodyJson(jsonNode)
                .uri(routes.RecipeController.createRecipe().url());

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals(TestUtils.TYPE_XML, result.contentType().get());
        // --------------------------------------------------------------
    }

    private void testRecipeJSONAttrs(Result result){
        testRecipeJSONAttrs(result,null);
    }

    private void testRecipeJSONAttrs(Result result, Recipe recipe ){
        assertEquals(TestUtils.TYPE_JSON, result.contentType().get());
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);
        
        assertNotNull( resultJson.get("id").asLong()                );
        assertNotNull( resultJson.get("name").asText()              );
        assertNotNull( resultJson.get("description").asText()       );
        assertNotNull( resultJson.get("numOfPeople").asInt()        );
        assertNotNull( resultJson.get("difficulty").asInt()         );
        assertNotNull( resultJson.get("price").floatValue()         );
        assertNotNull( resultJson.get("typeCourse").asInt()         );
        assertNotNull( resultJson.get("preparationTime").asText()   );
        assertNotNull( resultJson.get("ingredients").asText()       );
        assertNotNull( resultJson.get("stepsRecipe").asText()       );

        if( recipe!=null ){
            assertEquals( resultJson.get("id").asLong(),                recipe.getId().longValue()          );
            assertEquals( resultJson.get("name").asText(),              recipe.getName()                    );
            assertEquals( resultJson.get("description").asText(),       recipe.getDescription()             );
            assertEquals( resultJson.get("numOfPeople").asInt(),        recipe.getNumOfPeople().intValue()  );
            assertEquals( resultJson.get("difficulty").asInt(),         recipe.getDifficulty().intValue()   );
            assertEquals( resultJson.get("price").doubleValue(),        recipe.getPrice().doubleValue(), 0.0f     );
            assertEquals( resultJson.get("typeCourse").asInt(),         recipe.getTypeCourse().intValue()   );
            assertEquals( resultJson.get("preparationTime").asText(),   recipe.getPreparationTime()         );
        }
    }


    @Test
    public void getRecipeJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
            .method(GET)
            .uri(ROUTE_RECIPE+"/"+recipesList.get(0).getId())
            .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        testRecipeJSONAttrs(result, recipesList.get(0));
        // --------------------------------------------------------------
    }

    @Test
    public void getRecipeXMLTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_RECIPE+"/"+recipesList.get(0).getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        testRecipeXMLAttrs(recipesList.get(0));
        // --------------------------------------------------------------
    }


    private void testRecipeXMLAttrs(Recipe recipe){
        Content content = views.xml.recipe.render(recipe);
        assertEquals(TestUtils.TYPE_XML, content.contentType());
        validateRecipeXMLAttrs(contentAsString(content) ,recipe);
    }


    private void validateRecipeXMLAttrs(String content, Recipe recipe){
        assertTrue( content.contains("<recipe>")               );
        assertTrue( content.contains("<id>")                   );
        assertTrue( content.contains("<name>")                 );
        assertTrue( content.contains("<description>")          );
        assertTrue( content.contains("<numOfPeople>")          );
        assertTrue( content.contains("<difficulty>")           );
        assertTrue( content.contains("<price>")                );
        assertTrue( content.contains("<typeCourse>")           );
        assertTrue( content.contains("<preparationTime>")      );
        assertTrue( content.contains("<ingredients>")          );
        assertTrue( StringUtils.countMatches(content,"</ingredient>")==recipe.getIngredients().size() );
        assertTrue( content.contains("<steps>")                );
        assertTrue( StringUtils.countMatches(content,"</step>")==recipe.getStepsRecipe().size() );
    }


    @Test
    public void getAllRecipesJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_RECIPES)
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK,  result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_JSON, result.contentType().get() );
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        System.out.println( contentAsString(result) );

        // The following 3 attributes are shared by the Recipe, Ingredient and StepRecipe classes.
        // Therefore, it will be necessary to know the number of elements in each recipe
        assertTrue( StringUtils.countMatches(contentAsString(result),"id")==recipesList.size() + getTotalIngredients() + getTotalSteps() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"name")==recipesList.size() + getTotalIngredients());
        assertTrue( StringUtils.countMatches(contentAsString(result),"description")==recipesList.size() + getTotalIngredients() + getTotalSteps());

        assertTrue( StringUtils.countMatches(contentAsString(result),"numOfPeople")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"difficulty")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"typeCourse")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"preparationTime")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"price")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"ingredients")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"steps")==recipesList.size() );
        // --------------------------------------------------------------
    }

    @Test
    public void getAllRecipesXMLTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_RECIPES)
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals(OK,  result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        Content res = views.xml.recipeslist.render(recipesList);
        assertEquals(TestUtils.TYPE_XML, res.contentType());

        // The following 3 attributes are shared by the <Recipe>, <Ingredient> and <StepRecipe> classes.
        // Therefore, it will be necessary to know the number of elements in each recipe
        assertTrue(contentAsString(res).contains("<recipes>"));
        assertTrue( StringUtils.countMatches(contentAsString(res), "<recipe>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(res), "<id>")==recipesList.size() + getTotalIngredients() + getTotalSteps());
        assertTrue( StringUtils.countMatches(contentAsString(res), "<name>")==recipesList.size() + getTotalIngredients());
        assertTrue( StringUtils.countMatches(contentAsString(res), "<description>")==recipesList.size() + getTotalIngredients() + getTotalSteps());
        assertTrue( StringUtils.countMatches(contentAsString(result),"<numOfPeople>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<difficulty>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<typeCourse>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<preparationTime>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<price>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<ingredients>")==recipesList.size() );
        assertTrue( StringUtils.countMatches(contentAsString(result),"<steps>")==recipesList.size() );
    }


    @Test
    public void removeRecipeJSONTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(DELETE)
                .uri(ROUTE_RECIPE+"/"+recipesList.get(0).getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
    }

    private int getTotalIngredients(){
        int count = 0;
        if( recipesList!=null && !recipesList.isEmpty() ){
            for( Recipe recipe : recipesList ){
                if( recipe.getIngredients()!=null && !recipe.getIngredients().isEmpty() ){
                    count += recipe.getIngredients().size();
                }
            }
        }
        return count;
    }

    private int getTotalSteps(){
        int count = 0;
        if( recipesList!=null && !recipesList.isEmpty() ){
            for( Recipe recipe : recipesList ){
                if( recipe.getStepsRecipe()!=null && !recipe.getStepsRecipe().isEmpty() ){
                    count += recipe.getStepsRecipe().size();
                }
            }
        }
        return count;
    }

}
