package controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.routes;
import models.Ingredient;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import play.Application;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import play.twirl.api.Content;
import scala.io.Codec;
import scala.io.Source;
import utils.TestUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IngredientTest extends WithApplication {

    private static final String ROUTE_INGREDIENT  = "/ingredient";
    private static final String ROUTE_INGREDIENTS = "/ingredients";

    private Ingredient ingredient1, ingredient2;

    @Override
    protected Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    @Before
    public void setUpIngredientTest(){
        ingredient1 = TestUtils.createIngredientByNumber(1,true);
        ingredient2 = TestUtils.createIngredientByNumber(2,true);
    }

    @After
    public void tearDownIngredientTest(){
        if( ingredient1 !=null ){
            ingredient1 = null;
        }
        if( ingredient2!=null ) {
            ingredient2.delete();
            ingredient2 = null;
        }
    }

    @Test
    public void createIngredientJSONTest() throws IOException {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        String      jsonString  = Source.fromURL( getClass().getClassLoader().getResource(TestUtils.INGREDIENT_CREATE_JSON), Codec.defaultCharsetCodec() ).getLines().mkString();
        JsonNode    jsonNode    = (new ObjectMapper()).readTree(jsonString);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header(TestUtils.HEADER_CONTENT_TYPE,  TestUtils.TYPE_JSON)
                .header(TestUtils.HEADER_ACCEPT,        TestUtils.TYPE_JSON)
                .bodyJson(jsonNode)
                .uri(routes.IngredientController.createIngredient(-1L).url());

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals(TestUtils.TYPE_JSON, result.contentType().get());
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        assertNotNull( resultJson.get("id").asLong()    );
        assertNotNull( resultJson.get("name").asText()  );
        // --------------------------------------------------------------
    }

    @Test
    public void getIngredientJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_INGREDIENT+"/"+ ingredient1.getId().toString())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals( OK, result.status() );
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_JSON, result.contentType().get() );
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        assertNotNull( resultJson.get("id").asLong()            );
        assertNotNull( resultJson.get("name").asText()          );
        assertNotNull( resultJson.get("description").asText()   );

        assertEquals    ( new Long(resultJson.get("id").asLong()),  ingredient1.getId() );
        assertEquals    ( resultJson.get("name").asText(),          ingredient1.getName()  );
        assertEquals    ( resultJson.get("description").asText(),   ingredient1.getDescription()   );
        // --------------------------------------------------------------
    }


    @Test
    public void getIngredientXMLTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_INGREDIENT+"/"+ ingredient1.getId().toString())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals( OK, result.status() );
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        Content res = views.xml.ingredient.render(ingredient1);
        assertEquals(TestUtils.TYPE_XML, res.contentType());

        assertTrue(contentAsString(res).contains("<ingredient>"));
        assertTrue(contentAsString(res).contains("<id>"));
        assertTrue(contentAsString(res).contains("<name>"));
        assertTrue(contentAsString(res).contains("<description>"));
        // --------------------------------------------------------------
    }


    @Test
    public void getAllIngredientsJSONTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_INGREDIENTS)
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);

        assertEquals( OK, result.status() );
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_JSON, result.contentType().get() );
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);
        // --------------------------------------------------------------
    }

    @Test
    public void getAllIngredientsXMLTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_INGREDIENTS)
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);

        assertEquals( OK, result.status() );
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        List<Ingredient> list = new ArrayList();
        list.add(ingredient1);
        list.add(ingredient2);

        Content res = views.xml.ingredientslist.render(list);
        assertEquals(TestUtils.TYPE_XML, res.contentType());

        assertTrue(contentAsString(res).contains("<ingredients>"));
        assertTrue(contentAsString(res).contains("<ingredient>"));
        assertTrue(contentAsString(res).contains("<id>"));
        assertTrue(contentAsString(res).contains("<name>"));
        assertTrue(contentAsString(res).contains("<description>"));
        // --------------------------------------------------------------
    }

    @Test
    public void removeIngredientJSONTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(DELETE)
                .uri(ROUTE_INGREDIENT+"/"+ ingredient1.getId().toString())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
    }

}
