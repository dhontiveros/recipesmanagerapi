package controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.routes;
import models.Recipe;
import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runners.MethodSorters;
import play.Application;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import play.twirl.api.Content;
import scala.io.Codec;
import scala.io.Source;
import utils.TestUtils;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StepRecipeTest extends WithApplication {

    private static final String ROUTE_STEP  = "/step";
    private static final String ROUTE_STEPS = "/steps";

    private Recipe recipe1, recipe2;

    @Override
    protected Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    @Before
    public void setUpStepRecipe(){
        recipe1 = TestUtils.createRecipeFromNumber(1);
        recipe2 = TestUtils.createRecipeFromNumber(2);
    }

    @After
    public void tearDownStepRecipe(){
        if( recipe1!=null ){
            recipe1.delete();
        }
        recipe1 = null;
        if( recipe2!=null ){
            recipe2.delete();
        }
        recipe2 = null;
    }

    @Test
    public void addStepJSONTest() throws IOException {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        String  jsonString  = Source.fromURL( getClass().getClassLoader().getResource(TestUtils.STEP_ADD_JSON), Codec.defaultCharsetCodec() ).getLines().mkString();
        JsonNode jsonNode = (new ObjectMapper()).readTree(jsonString);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header(TestUtils.HEADER_CONTENT_TYPE,  TestUtils.TYPE_JSON)
                .header(TestUtils.HEADER_ACCEPT,        TestUtils.TYPE_JSON)
                .bodyJson(jsonNode)
                .uri(routes.StepRecipeController.addStep(recipe1.getId()).url());

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals(TestUtils.TYPE_JSON, result.contentType().get());
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        assertNotNull( resultJson.get("description").asText() );
        // --------------------------------------------------------------
    }

    @Test
    public void getStepJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEP+"/"+recipe1.getStepsRecipe().get(0).getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals(TestUtils.TYPE_JSON, result.contentType().get());
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        assertNotNull( resultJson.get("description").asText() );
        assertEquals( recipe1.getStepsRecipe().get(0).getDescription(), resultJson.get("description").asText() );
        // --------------------------------------------------------------
    }

    @Test
    public void getStepXMLTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEP+"/"+recipe1.getStepsRecipe().get(0).getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        Assert.assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        Content res = views.xml.step.render(recipe1.getStepsRecipe().get(0));
        Assert.assertEquals(TestUtils.TYPE_XML, res.contentType());

        assertTrue(contentAsString(res).contains("<step>"));
        assertTrue(contentAsString(res).contains("<id>"));
        assertTrue(contentAsString(res).contains("<order>"));
        assertTrue(contentAsString(res).contains("<description>"));
        // --------------------------------------------------------------
    }

    @Test
    public void getStepByOrderRecipeJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEP+"/0/"+recipe1.getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals(TestUtils.TYPE_JSON, result.contentType().get());
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);

        assertNotNull( resultJson.get("description").asText() );
        assertEquals( recipe1.getStepsRecipe().get(0).getDescription(), resultJson.get("description").asText() );
        // --------------------------------------------------------------
    }

    @Test
    public void getStepByOrderRecipeXMLTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEP+"/0/"+recipe1.getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        Assert.assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        Content res = views.xml.step.render(recipe1.getStepsRecipe().get(0));
        Assert.assertEquals(TestUtils.TYPE_XML, res.contentType());

        assertTrue(contentAsString(res).contains("<step>"));
        assertTrue(contentAsString(res).contains("<id>"));
        assertTrue(contentAsString(res).contains("<order>"));
        assertTrue(contentAsString(res).contains("<description>"));
        // --------------------------------------------------------------
    }

    @Test
    public void listStepsJSONTest() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEPS+"/"+recipe1.getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_JSON, result.contentType().get() );
        JsonNode resultJson = Json.parse(contentAsString(result));
        assertNotNull(resultJson);
        assertTrue( StringUtils.countMatches(contentAsString(result), "description")==recipe1.getStepsRecipe().size() );
        // --------------------------------------------------------------
    }




    @Test
    public void listStepsXMLTest(){
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(GET)
                .uri(ROUTE_STEPS+"/"+recipe1.getId())
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_XML);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
        // CONTROLLERS RESULT
        // --------------------------------------------------------------
        assertEquals( TestUtils.TYPE_XML, result.contentType().get() );
        // --------------------------------------------------------------
        // VIEWS
        // --------------------------------------------------------------
        Content res = views.xml.stepslist.render(recipe1.getStepsRecipe());
        assertEquals(TestUtils.TYPE_XML, res.contentType());

        assertTrue(contentAsString(res).contains("<steps>"));
        assertTrue(contentAsString(res).contains("<step>"));
        assertTrue(contentAsString(res).contains("<id>"));
        assertTrue(contentAsString(res).contains("<order>"));
        assertTrue(contentAsString(res).contains("<description>"));

        assertTrue( StringUtils.countMatches(contentAsString(result), "</step>")==recipe1.getStepsRecipe().size() );
        assertTrue( StringUtils.countMatches(contentAsString(result), "</id>")==recipe1.getStepsRecipe().size() );
        assertTrue( StringUtils.countMatches(contentAsString(result), "</order>")==recipe1.getStepsRecipe().size() );
        assertTrue( StringUtils.countMatches(contentAsString(result), "</description>")==recipe1.getStepsRecipe().size() );
        // --------------------------------------------------------------
    }


    @Test
    public void removeStep() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(DELETE)
                .uri(ROUTE_STEP+"/1")
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
    }

    @Test
    public void removeStepByOrderRecipe() {
        // --------------------------------------------------------------
        // ROUTES
        // --------------------------------------------------------------
        Http.RequestBuilder request = Helpers.fakeRequest()
                .method(DELETE)
                .uri(ROUTE_STEP+"/0/"+recipe1.getId()) // step/<order_step>/<id_recipe
                .header(TestUtils.HEADER_ACCEPT, TestUtils.TYPE_JSON);

        Result result = route(app, request);
        assertEquals(OK, result.status());
        // --------------------------------------------------------------
    }
}
